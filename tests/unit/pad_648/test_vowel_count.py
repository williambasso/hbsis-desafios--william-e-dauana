import unittest
from app.pad_648.vowel_count import VowelCount

class TestVogais(unittest.TestCase):
    def test_total_vowel_in_each_position(self):
        string = VowelCount('Eu sou uma frase aaa')
        dict = string.count_vowel_amount_in_each_position()
        self.assertDictEqual(dict, {'a': 5, 'e': 2, 'o': 1, 'u': 3})

    def test_total_vowels_in_string(self):
        string = VowelCount('Eu sou uma frase aaa')
        test = string.count_total_vowel()
        self.assertEqual(test, 11)

    def test_if_its_really_getting_word(self):
        string = VowelCount('Eu sou uma frase aaa')
        test = string.get_string()
        self.assertEqual(test, 'Eu sou uma frase aaa')

