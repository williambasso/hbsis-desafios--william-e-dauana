import unittest
from app.pad_646.middle_character import MiddleCharacter


class TestMiddleCharacter(unittest.TestCase):
    def test_should_return_one_letter_because_size_is_odd(self):
        word_size_odd = MiddleCharacter('asdcd')
        return_ = word_size_odd.get_character_middle_of_string()
        self.assertEqual(return_, 'd')

    def test_should_return_two_letter_because_size_is_even(self):
        word_size_even = MiddleCharacter('asdcda')
        return_ = word_size_even.get_character_middle_of_string()
        self.assertEqual(return_, 'dc')

    def test_should_return_one_letter_because_one_letter_was_passed(self):
        word = MiddleCharacter('a')
        return_= word.get_character_middle_of_string()
        self.assertEqual(return_, 'a')

    def test_should_receive_a_word(self):
        word = MiddleCharacter('palavral')
        self.assertEqual(word.get_value_of_word(), 'palavral')
