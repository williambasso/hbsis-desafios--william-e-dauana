import unittest
from app.pad_647.string import String


class TestString(unittest.TestCase):
    def test_string_should_have_value(self):
        string = String('ertyu')
        self.assertEqual(string.get_value(), 'ertyu')

    def test_string_should_multiplication_string(self):
        string = String('Erty')
        self.assertEqual(string.power_value(), 'E-Rr-Ttt-Yyyy')

    def test_string_should_be_only_string(self):
        string = String('qwert')
        self.assertTrue(string.is_str())

    def test_string_not_should_be_only_string(self):
        string = String(12334)
        self.assertFalse(string.is_str())