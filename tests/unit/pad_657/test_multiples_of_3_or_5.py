import unittest

from app.pad_657.exception import AtributeErrorMultiplesOf3Or5Exception
from app.pad_657.multiples_of_3_or_5 import MultiplesOf3Or5


class TestMultiplesOf3Or5(unittest.TestCase):

    def test_multiples_of_3_or_5(self):
        num = MultiplesOf3Or5(10)
        self.assertEqual(num.sum_multiples_of_3_or_5(), 23)

    def test_should_return_get_value(self):
        num = MultiplesOf3Or5(10)
        self.assertEqual(num.get_num(), 10)

    def test_if_message_exception_should_be_show_with_invalid_value_passed(self):
        with self.assertRaises(AtributeErrorMultiplesOf3Or5Exception)as ex:
            MultiplesOf3Or5('11')
        self.assertEqual(ex.exception.message,'Value error: not is int')




