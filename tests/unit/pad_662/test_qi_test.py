import unittest
from app.pad_662.qi_test import QiTest


class TestQi(unittest.TestCase):

    def setUp(self) -> None:
        self.n2 = QiTest('1 3 5 8 9 7')
        self.n3 = QiTest('1 9 5 5 8 7')

    def test_if_game_is_validating(self):
        n = QiTest('2 4 7 8 10')
        self.assertEqual(n.play_qi(), 3)
        self.assertEqual(self.n2.play_qi(), 4)
        self.assertEqual(self.n3.play_qi(), 5)

    def test_if_returns_the_numbers(self):
        n = QiTest('2 4 7 8 10')
        self.assertEqual(n.get_num(), '2 4 7 8 10')

