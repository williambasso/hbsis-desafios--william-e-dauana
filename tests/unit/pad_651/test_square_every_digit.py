import unittest
from app.pad_651.square_every_digit import SquareEveryDigit


class TestSquareEveryDigit(unittest.TestCase):

    def setUp(self) -> None:
        self.num2 = SquareEveryDigit(246)
        self.num3 = SquareEveryDigit(45)

    def test_should_return_square_every_digit(self):
        num = SquareEveryDigit(4325)
        self.assertEqual(num.square_every_digit(), 169425)
        self.assertEqual(self.num2.square_every_digit(), 41636)
        self.assertEqual(self.num3.square_every_digit(), 1625)


