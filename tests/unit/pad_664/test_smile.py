import unittest

from app.pad_664.smile import Smile


class TestSmile(unittest.TestCase):
    def setUp(self) -> None:
        self.smile2 = Smile([])
        self.smile3 = Smile([':]', ':[', ':-)', ':-D'])

    def test_if_count(self):
        smile = Smile([':D',':)', ':-)',':-D',':~D',';-D',':~)', ';D',';)',';~)',';~('])
        self.assertEqual(smile.count_smiles(), 10)
        self.assertEqual(self.smile2.count_smiles(), 0)
        self.assertEqual(self.smile3.count_smiles(), 2)

    def test_get_value(self):
        smile = self.smile2
        self.assertEqual(smile.get_list_of_smiles(), [])



