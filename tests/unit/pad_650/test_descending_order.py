import unittest
from app.pad_650.descending_order import DescendingOrder


class TestPad650(unittest.TestCase):
    def setUp(self) -> None:
        self.number2 = DescendingOrder(546)
        self.number3 = DescendingOrder(959)

    def test_should_have_value(self):
        number = DescendingOrder(1986)
        self.assertEqual(number.get_number(),1986)
        self.assertEqual(self.number2.get_number(),546)
        self.assertEqual(self.number3.get_number(),959)

    def test_should_show_number_in_desc_order(self):
        number = DescendingOrder(1986)
        self.assertEqual(number.return_number_in_descending_order(),9861)
        self.assertEqual(self.number2.return_number_in_descending_order(),654)
        self.assertEqual(self.number3.return_number_in_descending_order(),995)




