import unittest
from app.pad_661.your_request import *


class TestYourResquest(unittest.TestCase):

    def test_get_number_in_text_should_return_int(self):
        self.assertEqual(get_number_in_text('is2 Thi1s T4est 3a'),2)

    def test_should_return_sorting_by_number_in_text(self):
        self.assertEqual(sorting_by_number_in_text('is2 Thi1s T4est 3a'), 'Thi1s is2 3a T4est')


