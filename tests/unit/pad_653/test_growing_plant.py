import unittest
import pytest

from app.pad_653.growing_plant import GrowingPlant


class TestGrowingPlant(unittest.TestCase):
    def test_if_plant_is_growing(self):
        growing_plant = GrowingPlant(100, 10, 910)
        self.assertEqual(growing_plant.plant_growth(), 10)

    def setUp(self) -> None:
        self.plant_get_one = GrowingPlant(100, 10, 910)
        self.plant_get_two = GrowingPlant(100, 10, 910)
        self.plant_get_three = GrowingPlant(100, 10, 910)
        self.plant_upspeed_wrong = GrowingPlant(100, 10, 910)

    def test_should_return_get_plant(self):
        self.assertEqual(self.plant_get_one.get_upspeed(), 100)
        self.assertEqual(self.plant_get_two.get_downspeed(), 10)
        self.assertEqual(self.plant_get_three.get_desiredheight(), 910)

    def test_exception_upspeed(self):
        with pytest.raises(Exception) as ex:
            GrowingPlant(150, 10, 910)
        self.assertEqual(ex.value.args[0], 'Valor error: Upspeed precisa ser >= 5 ou ser <= 100')

    def test_exception_downspeed(self):
        with pytest.raises(Exception) as ex:
            GrowingPlant(100, 1, 910)
        self.assertEqual(ex.value.args[0], 'Value error: Downspeed precisa ser >= 2 ou menor que seu upspeed')

    def test_exception_desiredheight(self):
        with pytest.raises(Exception) as ex:
            GrowingPlant(100, 10, 1010)
        self.assertEqual(ex.value.args[0], 'Value error: Desiredheight precisa ser >= 4 ou <= 1000')




