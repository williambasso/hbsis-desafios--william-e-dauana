import unittest
from app.pad_649.shorter_word import ShorterWord


class TestShorterWord(unittest.TestCase):
    def setUp(self) -> None:
        self.word1 = ShorterWord('The bitcoin is over')
        self.word2 = ShorterWord('The bitcoin not issss over')
    def test_should_return_shorter_word(self):
        word = ShorterWord('Ola blabnlal dsasd vg arfdsg')
        self.assertEqual(word.shorter_word(),2)
        self.assertEqual(self.word1.shorter_word(), 2)
        self.assertEqual(self.word2.shorter_word(), 3)

    def test_if_get_return_value(self):
        word = ShorterWord('blablabla')
        self.assertEqual(word.get_word(), 'blablabla')