import unittest
from app.pad_652.double_sort import DoubleSort


class TestDoubleSort(unittest.TestCase):
    def test_should_return_double_sort(self):
        l = ["Banana", "Orange", "Apple", "Mango", 0, 2, 2, ]
        list = DoubleSort(l)
        self.assertEqual(list.double_sort(), [0, 2, 2, 'Apple', 'Banana', 'Mango', 'Orange'])

    def test_not_should_return_double_sort(self):
        l = ["Banana", "Orange", "Apple", "Mango", 0, 2, 2]
        list = DoubleSort(l)
        self.assertNotEqual(list.double_sort(), ["Banana", "Orange", "Apple", "Mango", 0, 2, 2])

    def test_should_return_list(self):
        l = ["Banana", "Orange", "Apple", "Mango", 0, 2, 2]
        list = DoubleSort(l)
        self.assertEqual(list.get_list_num_and_str(), ["Banana", "Orange", "Apple", "Mango", 0, 2, 2])



