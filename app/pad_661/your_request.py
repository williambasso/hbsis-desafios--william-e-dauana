def get_number_in_text(text: str) -> int:
    for char in text:
        if char.isnumeric():
            return int(char)

def sorting_by_number_in_text(text: str) -> str:
    organized_list = []
    for word in text.split():
        position = get_number_in_text(word)
        organized_list.insert(position-1, word)

    return ' '.join(organized_list)

print(sorting_by_number_in_text('is2 Thi1s T4est 3a'))
print(get_number_in_text('is2 Thi1s T4est 3a'))