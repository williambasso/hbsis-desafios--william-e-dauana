class VowelCount:
    def __init__(self, string: str):
        self._string = string

    def get_string(self) -> str:
        return self._string

    def count_vowel_amount_in_each_position(self) -> dict:
        string = self.get_string().lower()
        result = {}
        vowel = 'aeiou'
        for i in vowel:
            if i in string:
                result[i] = string.count(i)
        return result

    def count_total_vowel(self) -> int:
        vowel = 'aeiou'
        count = 0
        string = self.get_string().lower()
        for i in string:
            if i in vowel:
                count += 1
        return count

