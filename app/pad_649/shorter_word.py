class ShorterWord:
    def __init__(self, word: str):
        self._word = word

    def get_word(self) -> str:
        return self._word

    def shorter_word(self) -> int:
        list_of_words = self.get_word().split()
        shorter_word = len(list_of_words[0])
        for word in list_of_words:
            if len(word) < shorter_word:
                shorter_word = len(word)
        return shorter_word



