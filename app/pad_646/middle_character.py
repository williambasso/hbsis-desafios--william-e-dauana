class MiddleCharacter:
    def __init__(self, value: str):
        self._value = value

    def get_value_of_word(self) -> str:
        return self._value

    def get_character_middle_of_string(self) -> str:
        word = self.get_value_of_word()
        index = int(len(word) / 2)
        if len(word) % 2 == 1:
            return word[index]
        else:
            return word[index - 1] + word[index]















