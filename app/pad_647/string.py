class String:
    def __init__(self, value):
        self._value = value

    def get_value(self) -> str:
        return self._value

    def power_value(self) -> str:
        list_str = []
        c = 1
        for i in range(len(self.get_value())):
            list_str.append(self.get_value()[i] * c)
            c += 1
        list_str = [a.capitalize() for a in list_str]
        final_value = str('-'.join(list_str))
        return final_value

    def is_str(self) -> bool:
        if isinstance(self.get_value(), str):
            return True
        else:
            return False

