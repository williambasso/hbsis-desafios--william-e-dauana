class Smile:
    def __init__(self, list_of_smiles: list):
        self._list_of_smiles = list_of_smiles

    def get_list_of_smiles(self) -> list:
        return self._list_of_smiles

    def count_smiles(self):
        smiles = 0
        allowed_smiles = [':D',':)', ':-)',':-D',':~D',';-D',':~)', ';D',';)',';~)',';~D']
        list_of_smiles_of_user = self.get_list_of_smiles()
        for i in range(len(list_of_smiles_of_user)):
            if list_of_smiles_of_user[i] in allowed_smiles:
                smiles += 1
        if not len(list_of_smiles_of_user):
            return 0
        return smiles




