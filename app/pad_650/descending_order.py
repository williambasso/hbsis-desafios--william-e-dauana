class DescendingOrder:
    def __init__(self, number: int):
        self._number = number

    def get_number(self) -> int:
        return self._number

    def return_number_in_descending_order(self) -> int:
        list_numbers = []
        num = str(self.get_number())
        for i in range(len(num)):
            list_numbers.append(num[i])
        list_numbers.sort(reverse=True)
        final_number = int(''.join(list_numbers))
        return final_number













