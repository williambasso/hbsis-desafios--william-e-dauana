class QiTest:
    def __init__(self, num: str):
        self._num = num

    def get_num(self) -> str:
        return self._num

    def play_qi(self) -> int:
        num_odd = 0
        num_even = 0
        num = self.get_num().split()
        position = 0
        for i in range(0, len(num)):
            if int(num[i]) % 2 == 1:
                num_odd += 1
            else:
                num_even += 1
        if num_odd > num_even:
            for i in range(0, len(num)):
                if int(num[i]) % 2 == 0:
                    position = i + 1
        else:
            for i in range(0, len(num)):
                if int(num[i]) % 2 != 0:
                    position = i + 1
        return position




