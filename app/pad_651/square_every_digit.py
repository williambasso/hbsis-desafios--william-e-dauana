class SquareEveryDigit:
    def __init__(self, num: int):
        self._num = num

    def get_num(self) -> int:
        return self._num

    def square_every_digit(self) -> int:
        return_final = ''
        num_str = str(self.get_num())
        for index_num in num_str:
            calc = int(index_num) ** 2
            return_final += str(calc)
        return int(return_final)

