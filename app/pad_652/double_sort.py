class DoubleSort:
    def __init__(self, list_num_and_str: list):
        self._list_num_and_str = list_num_and_str

    def get_list_num_and_str(self) -> list:
        return self._list_num_and_str

    def double_sort(self) -> list:
        list_num = []
        list_str = []
        for i in self.get_list_num_and_str():
            if type(i) == int or type(i) == float:
                list_num.append(i)
            elif type(i) == str:
                list_str.append(i)
        list_num_sorted = sorted(list_num, key=int, reverse=False)
        list_str_sorted = sorted(list_str, key=str, reverse=False)
        return list_num_sorted + list_str_sorted












