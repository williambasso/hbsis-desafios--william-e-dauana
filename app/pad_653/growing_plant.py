class GrowingPlant:
    def __init__(self, upspeed, downspeed, desiredheight):
        self._upspeed = self._upspeed_validate_all(upspeed)
        self._downspeed = self._downspeed_validate_all(downspeed)
        self._desiredheight = self._desiredheight_validate_all(desiredheight)


    def _desiredheight_validate_all(self, desiredheight):
        if self._desiredheight(desiredheight):
            return desiredheight


    def _downspeed_validate_all(self, downspeed):
        if self._downspeed(downspeed):
            return downspeed


    def _upspeed_validate_all(self, upspeed):
        if self._upspeed(upspeed):
            return upspeed

    # Getters

    def get_upspeed(self):
        return self._upspeed

    def get_downspeed(self):
        return self._downspeed

    def get_desiredheight(self):
        return self._desiredheight

    # Verificadores

    def _upspeed(self, upspeed) -> int:
        min_value = 5
        max_value = 100
        if upspeed < min_value or upspeed > max_value:
            raise Exception('Valor error: Upspeed precisa ser >= 5 ou ser <= 100')
        return upspeed

    def _downspeed(self, downspeed) -> int:
        min_value = 2
        max_value = self.get_upspeed()
        if downspeed < min_value or downspeed > max_value:
            raise Exception('Value error: Downspeed precisa ser >= 2 ou menor que seu upspeed')
        return downspeed

    def _desiredheight(self, desiredheight) -> int:
        min_value = 4
        max_value = 1000
        if desiredheight < min_value or desiredheight > max_value:
            raise Exception('Value error: Desiredheight precisa ser >= 4 ou <= 1000')
        return desiredheight

    # Função principal

    def plant_growth(self):
        days = 1
        plant_size = 0
        while plant_size < self.get_desiredheight():
            plant_size += self.get_upspeed()
            if plant_size >= self.get_desiredheight():
                break
            plant_size -= self.get_downspeed()
            days += 1
        return days













