from app.pad_657.exception import AtributeErrorMultiplesOf3Or5Exception


class MultiplesOf3Or5:
    def __init__(self, num: int):
        self._num_verified(num)
        self._num = num

    @staticmethod
    def _num_verified(num: int):
        if not isinstance(num, int):
            raise AtributeErrorMultiplesOf3Or5Exception()
        return num

    def get_num(self) -> int:
        return self._num

    def sum_multiples_of_3_or_5(self) -> int:
        num = self.get_num()
        result = 0
        for i in range(0, num):
            if i % 3 == 0 or i % 5 == 0:
                result += i
        return result
